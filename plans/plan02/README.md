# Introduce the build with errors

The next step is to define the build for creating a docker image.

## Steps

* `cp .gitlab-ci.yml` to the root directory

## Result

* The build should fail, because there is no docker build possible without a Dockerfile.
* What else did change?